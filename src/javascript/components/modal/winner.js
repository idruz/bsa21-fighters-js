import { showModal } from './modal';

export function showWinnerModal(fighter) {
  // call showModal function 
  const img = document.createElement('img');
  img.src = fighter.source;

  showModal({
    title: `Winner - ${fighter.name}`,
    bodyElement: img,
    onClose: () => {
      location.reload();
    },
  });
}
