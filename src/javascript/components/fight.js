import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const fighterOne = {
      name: firstFighter.name,
      attack: firstFighter.attack,
      defense: firstFighter.defense,
      fullHealth: firstFighter.health,
      currentHealth: firstFighter.health,
      blocking: false
    };

    const fighterTwo = {
      name: secondFighter.name,
      attack: secondFighter.attack,
      defense: secondFighter.defense,
      fullHealth: secondFighter.health,
      currentHealth: secondFighter.health,
      blocking: false
    };

    const keyDownListener = (e) => {
      keyPressed(e.code, fighterOne, fighterTwo);

      if (fighterOne.currentHealth <= 0) {
        removeKeyListeners();
        resolve(secondFighter);
        console.log('secondFighter Win');
      } else if (fighterTwo.currentHealth <= 0) {
        removeKeyListeners();
        resolve(firstFighter);
        console.log('firstFighter Win');
      }
    };
    document.addEventListener('keydown', keyDownListener);

    const keyUpListener = (e) => {
      if (e.code === controls.PlayerOneBlock && fighterOne.blocking) {
        fighterOne.blocking = false;
      } else if (e.code === controls.PlayerTwoBlock && fighterTwo.blocking) {
        fighterTwo.blocking = false;
      }
    };
    document.addEventListener('keyup', keyUpListener);

    const removeKeyListeners = () => {
      document.removeEventListener('keydown', keyDownListener);
      document.removeEventListener('keyup', keyUpListener);
    };
  });
}

export function getDamage(attacker, defender) {
  // return damage
  if (attacker.blocking) {
    return 0;
  }

  if (!defender.blocking) {
    const damage = getHitPower(attacker) - getBlockPower(defender);

    return damage > 0 
      ? damage 
      : 0;
  } else {
    return 0;
  }
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = randomNumber(1, 2);
  const { attack } = fighter;

  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = randomNumber(1, 2);
  const { defense } = fighter;

  return defense * dodgeChance;
}

const keyPressed = (keyPress, fighterOne, fighterTwo) => {
  
  switch (keyPress) {
    case controls.PlayerOneAttack:
      const secondFighterDamageDealt = getDamage(fighterOne, fighterTwo);
      fighterTwo.currentHealth -= secondFighterDamageDealt;
      healthScale(fighterTwo, 'right');

      console.log('PlayerOneAttack', secondFighterDamageDealt, fighterTwo.currentHealth);
      break;
    case controls.PlayerTwoAttack:
      const firstFighterDamageDealt = getDamage(fighterOne, fighterTwo);
      fighterOne.currentHealth -= firstFighterDamageDealt;
      healthScale(fighterOne, 'left');

      console.log('PlayerTwoAttack', firstFighterDamageDealt, fighterOne.currentHealth);
      break;
    case controls.PlayerOneBlock:
      if (!fighterOne.blocking) {
        fighterOne.blocking = true;
      }
      console.log('PlayerOneBlock');
      break;
    case controls.PlayerTwoBlock:
      if (!fighterTwo.blocking) {
        fighterTwo.blocking = true;
      }
      console.log('PlayerTwoBlock');
      break;
    case controls.PlayerOneCriticalHitCombination: 
      console.log('PlayerOneCriticalHitCombination');
      break;
    case controls.PlayerOneCriticalHitCombination: 
      console.log('PlayerOneCriticalHitCombination');
      break;
    default:

      console.log('not found key pressed')

      break;
  }
}

//change health indicator width
const healthScale = (fighter, sideFighter) => {
  const sideFighterScale = document.getElementById(`${sideFighter}-fighter-indicator`);
  sideFighterScale.style.width = `${Math.round((fighter.currentHealth / fighter.fullHealth) * 100)}%`;
};

const randomNumber = (min, max) => {
  return Math.random() * (max - min) + min;
};